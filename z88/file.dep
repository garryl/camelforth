; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

if WORDSET_FILE

; ======================================================================
; CamelForth: ANS File (& Extension) wordsets (system-dependent words)
; System: Z88
; ======================================================================


; The Z88 versions of these words require the ZSTRING wordset, as the
; OS requires all filenames to be provided in null-terminated format.


; ======================================================================
; File-access methods
;   R/O, W/O and R/W return constants for file-access methods,
;                    used by OPEN-FILE and CREATE-FILE.
;   BIN modifies one of the above constants to indicate a binary file
;       (on most systems this does nothing).
; ======================================================================

;X R/O              -- fam                      Read-only file access method
;   1 CONSTANT R/O
    head RSLASHO,3,"R/O",docon,0,0
        defw 1

;X W/O              -- fam                      Write-only file access method
;   2 CONSTANT W/O
    head WSLASHO,3,"W/O",docon,0,0
        defw 2

;X R/W              -- fam                      Read/write file access method
;   3 CONSTANT R/W
    head RSLASHW,3,"R/W",docon,0,0
        defw 3

;X BIN              fam1 -- fam2                Modify file access method for binary files
;   ;
    head BIN,3,"BIN",docolon,1,0
        defw EXIT


; ======================================================================
; OPEN-FILE and CREATE-FILE
;   These open/create a file with the indicated access method, returning
;   a fileid (normally a filehandle) and error code ior (0=success).
;   CREATE-FILE should create an empty file if it doesn't already exist.
;   On the Z88, these will use the same OS function, so it is factored
;   out into an additional headerless word.
; ======================================================================

;X CREATE-FILE      caddr u fam -- fileid ior   Create new file
;   >R 2DUP W/O (OPEN-FILE) ?DUP
;   IF  2SWAP 2DROP R> DROP
;   ELSE  CLOSE-FILE DROP R> OPEN-FILE  THEN ;
     head CREATEFILE,11,"CREATE-FILE",docolon,0,0
        defw TOR,TWODUP,WSLASHO,BROPENFILE,QDUP
        defw QBRANCH,cfile2
        defw TWOSWAP,TWODROP,RFROM,DROP,EXIT
.cfile2 defw CLOSEFILE,DROP,RFROM,OPENFILE
.cfile3 defw EXIT

;X OPEN-FILE        caddr u fam -- fileid ior   Open existing file
;   DUP W/O = IF  DROP R/W  THEN
;   (OPEN-FILE) ;
    head OPENFILE,9,"OPEN-FILE",docolon,0,0
        defw DUP,WSLASHO,EQUAL
        defw QBRANCH,opfil2
        defw DROP,RSLASHW
.opfil2 defw BROPENFILE,EXIT

;- (OPEN-FILE)      caddr u fam -- fileid ior
    nohead BROPENFILE,docolon
        defw TOR,STRTONUL,RFROM,openf2
.openf2 ld a,c          ; A=access-mode
        pop hl
        ld bc,255       ; explicit name length 255, local filename
        push ix
        push de         ; save RP & IP
        ld de,usr_zpad  ; (place for expanded filename)
        call_oz(gn_opf)
        pop de
        ex (sp),ix      ; restore RP & IP; 2OS=fileid
.setior ld bc,0         ; set ior=0 (success)
        jp nc,EXIT      ; exit if no error
        ld b,$fe
        neg
        ld c,a          ; set ior=-256-A
        jp EXIT


; ======================================================================
; DELETE-FILE
;   Deletes the specified file
; ======================================================================

;X DELETE-FILE      caddr u -- ior              Delete file
    head DELETEFILE,11,"DELETE-FILE",docolon,0,0
        defw STRTONUL,dfile2
.dfile2 ld h,b
        ld l,c
        ld b,0
        call_oz(gn_del)
        jr setior       ; go to set result codes


; ======================================================================
; READ-FILE and WRITE-FILE
;   Reads/writes a block of data to the file, returning error in ior.
;   For READ-FILE, the results should be as follows:
;       ior<>0          error reading file (u2 bytes read so far)
;       ior=0, u2=u1    read successful
;       ior=0, u2<u1    read successful, but end-of-file after u2 bytes
;       ior=0, u2=0     end-of-file already reached
; ======================================================================

;X READ-FILE        caddr u1 fileid -- u2 ior   Read data from file
    head READFILE,9,"READ-FILE",docode,0,0
        ex de,hl
        pop de
        ex (sp),hl      ; BC=fileid, DE=u1, HL=caddr, IP saved on stack
        push bc
        ex (sp),ix      ; IX=fileid, DE=u1, HL=caddr, IP & RP saved on stack
        ld b,d
        ld c,e          ; BC=length
        push bc         ; save length for later
        ld de,0
        ex de,hl        ; HL=0, DE=caddr - move from file to memory
        call_oz(os_mv)
        pop hl          ; HL=original length
        pop ix          ; restore RP
        pop de          ; and IP
        call vector     ; execute any appropriate error vector
        push af         ; save error code
        and a
        sbc hl,bc       ; HL=length moved
        pop af
        push hl         ; 2OS=length moved
        jp nc,setior2   ; exit if no error
        cp 9
        jp z,setior2    ; or if EOF (carry reset)
        scf
        jp setior2      ; else set error

;X WRITE-FILE       caddr u1 fileid -- ior      Write data to file
    head WRITEFILE,10,"WRITE-FILE",docode,0,0
        ex de,hl
        pop de
        ex (sp),hl      ; BC=fileid, DE=u1, HL=caddr, IP saved on stack
        push bc
        ex (sp),ix      ; IX=fileid, DE=u1, HL=caddr, IP & RP saved on stack
        ld b,d
        ld c,e          ; BC=length
        ld de,0         ; DE=0, HL=caddr - move from memory to file
        call_oz(os_mv)
        pop ix          ; restore RP
        pop de          ; and IP
        call vector     ; execute any appropriate error vector
        jp setior2      ; go to set success/error


; ======================================================================
; READ-LINE and WRITE-LINE
;   WRITE-LINE simply writes the data specified followed by the system-
;   dependent end-of-line sequence, and may be defined in terms of
;   WRITE-FILE.
;   READ-LINE reads a maximum of u1 characters of data, terminating if
;   the system-dependent end-of-line sequence is encountered first. Its
;   results should be:
;       ior<>0                  error reading file (u2 bytes read so far)
;       ior=0, flag=true, u2=u1 read successful, line terminator not found
;       ior=0, flag=true, u2<u1 read successful, line terminator found
;       ior=0, flag=false, u2=0 end-of-file already reached
;   The line terminator should *not* be included in the count u2, although
;   it may be appended to the end of the buffer. If your system doesn't
;   include a suitable call it's probably worth modifying the Z88 version,
;   which accepts either CR or LF as the line terminator.
; ======================================================================

;X READ-LINE        caddr u1 fileid -- u2 f ior Read line from file
    head READLINE,9,"READ-LINE",docode,0,0
        ex de,hl
        pop de
        ex (sp),hl
        push bc
        ex (sp),ix      ; IX=fileid, DE=u1, HL=caddr, IP & RP saved on stack
        push de         ; save max length for later
.rdllp  ld a,d
        or e
        jr z,endrl      ; move on if no more chars to read
        call_oz(os_gb)
        jr c,rlerr      ; if error, move on
        dec de
        ld (hl),a
        inc hl
        cp 13
        jr z,iseol      ; move on if EOL char (CR)
        cp 10
        jr nz,rdllp     ; loop back if not end-of-line (LF)
.iseol  inc de          ; ignore eol terminator in calculations
        pop hl
        sbc hl,de       ; HL=chars read
.rdlok  pop ix          ; restore RP
        pop de          ; restore IP
        push hl         ; 3OS=u2
        ld hl,-1
        push hl         ; 2OS=true
        ld bc,0
        jr exior        ; exit with no error
.endrl  pop hl          ; HL=chars to read=chars read
        jr rdlok
.rlerr  pop hl
        push af
        and a
        sbc hl,de       ; HL=actual chars read
        pop af
        pop ix          ; restore RP
        pop de          ; restore IP
        call vector     ; execute any appropriate error vector
        push hl         ; 3OS=u2
        cp 9
        jr z,rleof      ; move on if EOF error
.rlfail ld hl,0
        push hl         ; else flag=FALSE
        jr setior2
.rleof  ld a,h
        or l
        ld a,9
        jr nz,rlfail    ; fail if some chars were read
        push hl         ; special case: u2=0, flag=0 and ior=0
        ld b,h
        ld c,l
        jr exior

;X WRITE-LINE       caddr u1 fileid -- ior      Write line to file
    head WRITELINE,10,"WRITE-LINE",docode,0,0
        ex de,hl
        pop de
        ex (sp),hl
        push bc
        ex (sp),ix      ; IX=fileid, DE=u1, HL=caddr, IP & RP saved on stack
        ld b,d
        ld c,e
        ld de,0
        call_oz(os_mv)
        jr c,wtlerr     ; move on if error
        ld a,13
        call_oz(os_pb)
.wtlerr pop ix          ; restore RP
        pop de          ; restore IP
        call vector     ; execute any appropriate error vector
        jr setior2      ; go to set success/error


; ======================================================================
; CLOSE-FILE
;   Closes the file specified
; ======================================================================

;X CLOSE-FILE       fileid -- ior               Close file
    head CLOSEFILE,10,"CLOSE-FILE",docode,0,exior-cfilef
.cfilef push bc
        ex (sp),ix      ; save RP, get IX=fileid
        call_oz(gn_cl)
        pop ix          ; restore RP
.setior2 ld bc,0        ; set ior=0 (success)
        jr nc,exior     ; exit if no error
        ld b,$fe
        neg
        ld c,a          ; set ior=-256-A
.exior
        next


; ======================================================================
; FILE-POSITION and FILE-SIZE
;   Return the current file position and size, respectively, as a double
;   number.
; ======================================================================

;X FILE-POSITION    fileid -- ud ior            Get file position
    head FILEPOSITION,13,"FILE-POSITION",docode,0,0
        ld a,1          ; reason: FA_PTR
.osfrm  push de         ; save IP
        push bc
        ex (sp),ix      ; save RP, IX=fileid
        ld de,0
        call_oz(os_frm)
        ex de,hl        ; HLBC=result
        pop ix
        pop de          ; restore RP & IP
        push bc
        push hl         ; stack result
        jr setior2      ; go to set result codes

;X FILE-SIZE        fileid -- ud ior            Get file size
    head FILESIZE,9,"FILE-SIZE",docode,0,0
        ld a,2          ; reason: FA_EXT
        jr osfrm


; ======================================================================
; REPOSITION-FILE and RESIZE-FILE
;   Sets the current file position and size, respectively, as a double
;   number. If your OS doesn't support resizing, just return an error
;   value (on the Z88 we can only resize if extending, not truncating).
; ======================================================================

;X REPOSITION-FILE  ud fileid -- ior            Set file position
    head REPOSITIONFILE,15,"REPOSITION-FILE",docode,0,0
        ld a,1          ; reason=FA_PTR
.osfwm  pop hl
        ld (ix-1),h
        ld (ix-2),l
        pop hl
        ld (ix-3),h
        ld (ix-4),l     ; place position on return stack
        push bc
        ex (sp),ix      ; IX=fileid
        pop hl
        push hl         ; save RP
        dec hl
        dec hl
        dec hl
        dec hl          ; HL=pointer to 4-byte value
        call_oz(os_fwm)
        pop ix          ; restore RP
        jr setior2      ; go to set success/error

; NB: This is a Z88-specific non-standard word and doesn't need a header probably
;Z EXTEND-FILE      ud fileid -- ior            Extend file
    head EXTENDFILE,11,"EXTEND-FILE",docode,0,0
        ld a,2          ; reason=FA_EXT
        jr osfwm

;X RESIZE-FILE      ud fileid -- ior            Change file size
;   DUP >R FILE-SIZE ?DUP
;   IF  R> DROP >R 2DROP 2DROP R> EXIT  THEN
;   2OVER 2SWAP DU<
;   IF  2DROP R> DROP -279 EXIT  THEN           ; RC_FAIL if want to shorten
;   R> EXTEND-FILE ;
    head RESIZEFILE,11,"RESIZE-FILE",docolon,0,0
        defw DUP,TOR,FILESIZE,QDUP
        defw QBRANCH,resiz2
        defw RFROM,DROP,TOR,TWODROP,TWODROP,RFROM,EXIT
.resiz2 defw TWOOVER,TWOSWAP,DULESS
        defw QBRANCH,resiz3
        defw TWODROP,RFROM,DROP,LIT,-279,EXIT
.resiz3 defw RFROM,EXTENDFILE,EXIT


; ======================================================================
; FILE-STATUS, FLUSH-FILE and RENAME-FILE
;   These last three words are the File Extension wordset, and thus are
;   pretty much optional.
; FILE-STATUS returns system-specific information on a file (on the Z88,
;   we give the creation and update times & dates)
; FLUSH-FILE attempts to flush all buffered information (on the Z88,
;   does nothing)
; RENAME-FILE renames a file
; ======================================================================

;X FILE-STATUS      caddr u -- x ior            Get file status
;  For this Z88 implementation, x is an address of a data structure
;  holding creation & update time & dates, in internal machine format  
;  This may be overwritten on pre-emption or with use of other words
;  that access OZ, so should be moved elsewhere if required
;  Typically, you would immediately convert it to time & date on
;  the stack with INT>T&D
    head FILESTATUS,11,"FILE-STATUS",docolon,0,0
        defw LIT,6,BROPENFILE                   ; open DOR handle
        defw DUP,QBRANCH,fstat1,EXIT
.fstat1 defw fstat2
.fstat2 ex (sp),ix              ; save RP, get IX=DOR handle
        push de                 ; save IP
        ld de,usr_zpad
        ld bc,$4306             ; creation time
        ld a,dr_rd
        call_oz(os_dor)
        ld de,usr_zpad+6
        ld bc,$5506             ; update time
        ld a,dr_rd              ; DR_RD
        call_oz(os_dor)
        ld a,dr_fre
        call_oz(os_dor)
        pop de                  ; restore IP
        ld ix,usr_zpad
        ex (sp),ix              ; restore RP, put 2OS=x
        ld bc,0                 ; TOS=0, file exists
        jp EXIT

;X FLUSH-FILE       fileid -- ior               Flush data to file
;   FILE-POSITION >R 2DROP R> ;         just checks fileid is valid
    head FLUSHFILE,10,"FLUSH-FILE",docolon,0,0
        defw FILEPOSITION,TOR,TWODROP,RFROM,EXIT

;X RENAME-FILE      caddr1 u1 caddr2 u2 -- ior  Rename file
;   127 MIN STR>NUL DUP 128 + 128 CMOVE
;   127 MIN STR>NUL <<code>> ;
    head RENAMEFILE,11,"RENAME-FILE",docolon,0,0
        defw LIT,127,MIN,STRTONUL
        defw DUP,LIT,128,PLUS,LIT,128,CMOVE
        defw LIT,127,MIN,STRTONUL
        defw renam1
.renam1 push de                 ; save IP
        ld h,b
        ld l,c
        ld b,0                  ; BHL=address of original name
        ld de,usr_zpad+128      ; DE=address of new name
        call_oz(gn_ren)
        pop de                  ; restore IP
        jp setior               ; go to set success/error


endif ; WORDSET_FILE
