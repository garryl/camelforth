; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth Extras - Z88 version
; ======================================================================

; This file defines extra modules for the Z88 version of CamelForth.
; WORDSET_ZSTRING is required for standard ANS file wordset on the Z88.


define  Z88_OZFEATURES          ; Requires WORDSET_ZSTRING
define  Z88_GRAPHICS            ; Requires Z88_OZFEATURES & WORDSET_TRIG
define  Z88_WINDOWS
define  Z88_FARMEM
define  Z88_PACKAGES

; Define this to build a non-working test binary to check size
;define Z88_BINSIZER


; ======================================================================
; DO NOT MODIFY BEYOND HERE
; ======================================================================

; A project file is now built based on the options selected

if BUILD_PRJFILE


if Z88_OZFEATURES
        defm    "ozfeats.z88"&$0d&$0a
endif
if Z88_GRAPHICS
        defm    "graphics.z88"&$0d&$0a
endif
if Z88_WINDOWS
        defm    "windows.z88"&$0d&$0a
endif
if Z88_FARMEM
        defm    "farmem.z88"&$0d&$0a
endif
if Z88_PACKAGES
        defm    "packages.z88"&$0d&$0a
endif
        defm    "ozsupport.z88"&$0d&$0a      
        defm    "romheader.z88"&$0d&$0a

endif ; BUILD_PRJFILE
