\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

\ Typical usage is:
\   S" outputfile" expmacros listfile

VOCABULARY MACROS
ONLY FORTH ALSO MACROS

0 VALUE outfile
0 VALUE infile
0 VALUE link
0 VALUE elink
0 VALUE savedlink
0 VALUE savedelink
VARIABLE #line
CREATE linebuffer 258 ALLOT
CREATE p1 32 ALLOT
CREATE p2 32 ALLOT
CREATE p3 32 ALLOT
CREATE p4 32 ALLOT
CREATE p5 32 ALLOT
CREATE p6 32 ALLOT

-13 CONSTANT undef-exc
-5000 CONSTANT endm-exc

\ Useful words for macros to use

: getparam ( "ccc" c-addr -- )
    >R
    [CHAR] , WORD COUNT -TRAILING
    DUP 0= IF  ." Missing parameter in line " #line @ . ABORT  THEN
    DUP R@ C!
    R> CHAR+ SWAP CMOVE ;

: paramvalue ( s-addr -- u )
    0 0 ROT COUNT >NUMBER 2DROP DROP ;

: value$ ( n -- c-addr u )
    0 <# #S #> ;

: endparams
    BL WORD COUNT -TRAILING
    0= SWAP C@ [CHAR] ; = OR
    0= IF  ." Too many parameters in line " #line @ . ABORT  THEN ;

: putline ( c-addr u -- )
    outfile WRITE-LINE THROW ;

: getline ( -- c-addr u f )
    linebuffer DUP 256 infile READ-LINE THROW ;

: putlabel ( c-addr u -- )
    [CHAR] . linebuffer C!
    >R linebuffer CHAR+ R@ CMOVE
    linebuffer R> 1+ putline ;

: putstrings ( c-addr1 u1 c-addr2 u2 -- )
    2>R DUP >R
    linebuffer SWAP CMOVE
    R> DUP CHARS linebuffer + 2R> DUP >R ROT SWAP CMOVE
    linebuffer SWAP R> + putline ;

: putaction ( c-addr u -- )
    2DUP S" docode" COMPARE
    IF  S" call " 2OVER putstrings
        2DUP S" docolon" COMPARE 0= >R
        2DUP S" docon" COMPARE 0= >R
        2DUP S" dosubsys" COMPARE 0= >R
        2DUP S" douser" COMPARE 0= >R
        2DUP S" dodefer" COMPARE 0=
        R> OR R> OR R> OR R> OR 0=
        IF  S" defw cforigin + (ASMPC+2)" putline  THEN
    THEN
    2DROP ;

\ The macros

MACROS DEFINITIONS

: next
    endparams
    S" jp next_code" putline 
    endm-exc THROW ;

: nexthl
    endparams
    S" jp nexthl_code" putline
    endm-exc THROW ;

: innext
    endparams
    S" ex de,hl" putline
    S" ld e,(hl)" putline
    S" inc hl" putline
    S" ld d,(hl)" putline
    S" inc hl" putline
    S" ex de,hl" putline
    S" jp (hl)" putline
    endm-exc THROW ;

: envhead
    p1 getparam         \ namelength
    p2 getparam         \ name
    p3 getparam         \ size
    endparams
    S" .env_link_" elink 1+ value$ putstrings
    S" defb " p1 paramvalue value$ putstrings
    S" defm " p2 COUNT putstrings
    elink IF  S" defw env_link_" elink value$ putstrings
          ELSE  S" defw 0" putline
          THEN
    elink 1+ TO elink
    S" defw " p3 paramvalue value$ putstrings
    endm-exc THROW ;
    
: head
    p1 getparam         \ label
    p2 getparam         \ namelength 
    p3 getparam         \ name
    p4 getparam         \ action
    p5 getparam         \ immed
    p6 getparam         \ fast
    endparams
    link IF  S" defw word_link_" link value$ putstrings
         ELSE  S" defw 0" putline
         THEN
    link 1+ TO link
    S" .word_link_" link value$ putstrings
    S" defb " p2 paramvalue value$ putstrings
    S" defm " p3 COUNT putstrings
    p5 paramvalue IF  S" defb 128+"  ELSE  S" defb "  THEN
    p6 COUNT putstrings
    p1 COUNT putlabel
    p4 COUNT putaction
    endm-exc THROW ;

: nohead
    p1 getparam         \ label
    p2 getparam         \ action
    endparams
    S" defb 0" putline
    p1 COUNT putlabel
    p2 COUNT putaction
    endm-exc THROW ;

: endforth
    endparams
    link IF  S" defc lastword = cforigin + word_link_" link value$ putstrings
         ELSE  S" defc lastword = 0" putline
         THEN
    elink IF  S" defc lastenv = cforigin + env_link_" link value$ putstrings
          ELSE  S" defc lastenv = 0" putline
          THEN
    S" defc enddict = cforigin + ASMPC" putline
    endm-exc THROW ;

\ The file processing words

FORTH DEFINITIONS

: process-file
    0 #line !
    BEGIN
      getline 1 #line +!
    WHILE
      ONLY MACROS
      2DUP ['] EVALUATE CATCH DUP undef-exc = 
      ONLY FORTH
      IF  DROP 2DROP putline
      ELSE  DUP endm-exc =
            IF  DROP 2DROP  ELSE  THROW  THEN  2DROP
      THEN
    REPEAT  2DROP ;

: expmacros ( "listfile" c-addr u -- )
    0 TO link  0 TO elink
    W/O CREATE-FILE THROW TO outfile
    BL WORD COUNT R/O OPEN-FILE THROW >R
    BEGIN
      linebuffer DUP 256 R@ READ-LINE THROW
    WHILE
      -TRAILING DUP
      IF  ." Processing: " 2DUP TYPE CR
          R/O OPEN-FILE THROW TO infile
          process-file
          infile CLOSE-FILE THROW
      ELSE  2DROP
      THEN
    REPEAT  2DROP
    R> CLOSE-FILE THROW
    outfile CLOSE-FILE THROW ;

