; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: Base System: Memory & Strings (low-level words)
; ======================================================================


;- !DEST   dest adrs --    change a branch dest'n
; Changes the destination address found at 'adrs'
; to the given 'dest'.  On the Z80 this is '!'
; ...other CPUs may need relative addressing.

;C !                x a-addr --                 Store cell in memory
    head STORE,1,"!",docode,0,storen-storef
.STOREDEST
.storef ld h,b          ; address in hl
        ld l,c
        pop bc          ; data in bc
        ld (hl),c
        inc hl
        ld (hl),b
        pop bc          ; pop new TOS
.storen
        next

;C C!               char c-addr --              Store char in memory
    head CSTORE,2,"C!",docode,0,cstorn-cstorf
.cstorf ld h,b          ; address in hl
        ld l,c
        pop bc          ; data in bc
        ld (hl),c
        pop bc          ; pop new TOS
.cstorn
        next

;C @                a-addr -- x                 Fetch cell from memory
    head FETCH,1,"@",docode,0,fetchn-fetchf
.fetchf ld h,b          ; address in hl
        ld l,c
        ld c,(hl)
        inc hl
        ld b,(hl)
.fetchn
        next

;C C@               c-addr -- char              Fetch char from memory
    head CFETCH,2,"C@",docode,0,cfetcn-cfetcf
.cfetcf ld a,(bc)
        ld c,a
        ld b,0
.cfetcn
        next

;Z PC!              char c-addr --              Output char to port
    head PCSTORE,3,"PC!",docode,0,pcston-pcstof
.pcstof pop hl          ; char in L
        out (c),l       ; to port (BC)
        pop bc          ; pop new TOS
.pcston
        next

;Z PC@              c-addr -- char              Input char from port
    head PCFETCH,3,"PC@",docode,0,pcfetn-pcfetf
.pcfetf in c,(c)        ; read port (BC) to C
        ld b,0
.pcfetn
        next


;C +!               n/u a-addr --               Add cell to memory
    head PLUSSTORE,2,"+!",docode,0,plstn-plstf
.plstf  pop hl
        ld a,(bc)       ; low byte
        add a,l
        ld (bc),a
        inc bc
        ld a,(bc)       ; high byte
        adc a,h
        ld (bc),a
        pop bc          ; pop new TOS
.plstn
        next

;C 2@               a-addr -- x1 x2             Fetch 2 cells
;   DUP CELL+ @ SWAP @ ;
;   the lower address will appear on top of stack
    head TWOFETCH,2,"2@",docode,0,twoftn-twoftf
.twoftf ld h,b
        ld l,c
        ld c,(hl)
        inc hl
        ld b,(hl)
        inc hl
        ld a,(hl)
        inc hl
        ld h,(hl)
        ld l,a
        push hl
.twoftn
        next

;C 2!               x1 x2 a-addr --             Store 2 cells
;   SWAP OVER ! CELL+ ! ;
;   the top of stack is stored at the lower adrs
    head TWOSTORE,2,"2!",docode,0,twostn-twostf
.twostf ld h,b
        ld l,c
        pop bc
        ld (hl),c
        inc hl
        ld (hl),b
        inc hl
        pop bc
        ld (hl),c
        inc hl
        ld (hl),b
        pop bc
.twostn
        next

;C FILL             c-addr u char --            Fill memory with char
    head FILL,4,"FILL",docode,0,filln-fillf
.fillf  ld a,c          ; character in a
        exx             ; use alt. register set
        pop bc          ; count in bc
        pop de          ; address in de
        or a            ; clear carry flag
        ld hl,$ffff
        adc hl,bc       ; test for count=0 or 1
        jr nc,filldone  ;   no cy: count=0, skip
        ld (de),a       ; fill first byte
        jr z,filldone   ;   zero, count=1, done
        dec bc          ; else adjust count,
        ld h,d          ;   let hl = start adrs,
        ld l,e
        inc de          ;   let de = start adrs+1
        ldir            ;   copy (hl)->(de)
.filldone exx           ; back to main reg set
        pop bc          ; pop new TOS
.filln
        next

;X CMOVE            c-addr1 c-addr2 u --        Move from bottom
; as defined in the ANSI optional String word set
; On byte machines, CMOVE and CMOVE> are logical
; factors of MOVE.  They are easy to implement on
; CPUs which have a block-move instruction.
    head CMOVE,5,"CMOVE",docode,0,cmoven-cmovef
.cmovef push bc
        exx
        pop bc      ; count
        pop de      ; destination adrs
        pop hl      ; source adrs
        ld a,b      ; test for count=0
        or c
        jr z,cmovedone
        ldir        ; move from bottom to top
.cmovedone exx
        pop bc      ; pop new TOS
.cmoven
        next

;X CMOVE>           c-addr1 c-addr2 u --        Move from top
; as defined in the ANSI optional String word set
    head CMOVEUP,6,"CMOVE>",docode,0,cmovun-cmovuf
.cmovuf push bc
        exx
        pop bc      ; count
        pop hl      ; destination adrs
        pop de      ; source adrs
        ld a,b      ; test for count=0
        or c
        jr z,umovedone
        add hl,bc   ; last byte in destination
        dec hl
        ex de,hl
        add hl,bc   ; last byte in source
        dec hl
        lddr        ; move from top to bottom
.umovedone exx
        pop bc      ; pop new TOS
.cmovun
        next


;Z SCAN             c-addr u c -- c-addr' u'    Find matching char
;  If scanning for BLs, all control characters match too
    head SCAN,4,"SCAN",docode,0,0
        ld a,c      ; scan character
        exx
        pop bc      ; count
        pop hl      ; address
        ld e,a      ; test for count=0
        ld a,b
        or c
        jr z,scandone
        ld a,e
        cp $20          ; are we scanning for blanks+control chars?
        jr z,scanbllp
        cpir        ; scan 'til match or count=0
        jr nz,scandone  ; no match, BC & HL ok
        inc bc          ; match!  undo last to
        dec hl          ;   point at match char
.scandone push hl   ; updated address
        push bc     ; updated count
        exx
        pop bc      ; TOS in bc
        next
.scanbllp ld a,$20
        cp (hl)
        jr nc,scandone  ; done if character<=20h found
        inc hl
        dec bc
        ld a,b
        or c
        jr nz,scanbllp  ; loop back for more chars
        jr scandone


;Z S=               c-addr1 c-addr2 u -- n      String compare: n<0: s1<s2, n=0: s1=s2, n>0: s1>s2
    head SEQUAL,2,"S=",docode,0,0
        push bc
        exx
        pop bc      ; count
        pop hl      ; addr2
        pop de      ; addr1
        ld a,b      ; test for count=0
        or c
        jr z,smatch     ; by definition, match!
.sloop  ld a,(de)
        inc de
        cpi
        jr nz,sdiff     ; char mismatch: exit
        jp pe,sloop     ; count not exhausted
.smatch ; count exhausted & no mismatch found
        exx
        ld bc,0         ; bc=0000  (s1=s2)
        jr snext
.sdiff  ; mismatch!  undo last 'cpi' increment
        dec hl          ; point at mismatch char
        cp (hl)         ; set cy if char1 < char2
        sbc a,a         ; propagate cy thru A
        exx
        ld b,a          ; bc=FFFF if cy (s1<s2)
        or 1            ; bc=0001 if ncy (s1>s2)
        ld c,a
.snext
        next


;Z >UPPER           c-addr u --                 Convert string to uppercase
     head TOUPPER,6,">UPPER",docode,0,tupprn-tupprf
.tupprf pop hl          ; HL=address, BC=count
.tupp0  ld a,b
        or c
        jr z,tupp2      ; end of string reached
        ld a,(hl)
        cp 'a'
        jr c,tupp1
        cp '{'
        jr nc,tupp1
        and 223         ; convert lowercase letters only
        ld (hl),a
.tupp1  inc hl
        dec bc
        jr tupp0
.tupp2  pop bc          ; get new TOS
.tupprn
        next

;Z >LOWER           c-addr u --                 Convert string to lowercase
     head TOLOWER,6,">LOWER",docode,0,tlowrn-tlowrf
.tlowrf pop hl          ; HL=address, BC=count
.tlow0  ld a,b
        or c
        jr z,tlow2      ; end of string reached
        ld a,(hl)
        cp 'A'
        jr c,tlow1
        cp '['
        jr nc,tlow1
        or 32           ; convert uppercase letters only
        ld (hl),a
.tlow1  inc hl
        dec bc
        jr tlow0
.tlow2  pop bc
.tlowrn
        next


;C COUNT            c-addr1 -- c-addr2 u        Counted string->addr/len
;   DUP CHAR+ SWAP C@ ;
    head COUNT,5,"COUNT",docode,0,counn-counf
.counf  ld a,(bc)
        inc bc
        push bc
        ld c,a
        ld b,0
.counn
        next

