; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

if WORDSET_SEARCHORDER

; ======================================================================
; CamelForth: ANS Search-Order (& Extension) wordsets (high-level words)
; ======================================================================


;- NEXTLIST         -- a-addr                   Next available wid
    nohead NEXTLIST,docon
        defw usr_nextlist

;- SORDER           -- a-addr                   Search-order specification
    nohead SORDER,docon
        defw usr_sorder

;X FORTH-WORDLIST   -- wid                      Wordlist ID for all FORTH words
    head FORTHWORDLIST,14,"FORTH-WORDLIST",docon,0,0
        defw usr_forth

;X WORDLIST         -- wid                      Get new wordlist ID
;   NEXTLIST @
;   1 CELLS NEXTLIST +!
;   0 OVER ! ;
    head WORDLIST,8,"WORDLIST",docolon,0,0
        defw NEXTLIST,FETCH
        defw ONE,CELLS,NEXTLIST,PLUSSTORE
        defw ZERO,OVER,STORE,EXIT

;X DEFINITIONS      --                          Set current to first in search order
;   SORDER @ IF  SORDER CELL+ @ SET-CURRENT  THEN ;
    head DEFINITIONS,11,"DEFINITIONS",docolon,0,0
        defw SORDER,FETCH,QBRANCH,defs1
        defw SORDER,CELLPLUS,FETCH,SETCURRENT
.defs1  defw EXIT

;X GET-ORDER        -- widn...wid1 n            Get search order
;   SORDER @ DUP IF
;     1 SWAP DO  SORDER I CELLS + @  -1 +LOOP
;     SORDER @
;   THEN ;
    head GETORDER,9,"GET-ORDER",docolon,0,0
        defw SORDER,FETCH,DUP,QBRANCH,getor2
        defw ONE,SWOP,XDO
.getor1 defw SORDER,II,CELLS,PLUS,FETCH
        defw MINUSONE,XPLUSLOOP,getor1
        defw SORDER,FETCH
.getor2 defw EXIT

;X SET-ORDER        widn...wid1 n --            Set search order
;   CASE  -1 OF  FORTH-WORDLIST 1 RECURSE  ENDOF
;          0 OF  0 SORDER !  ENDOF
;   DUP >R
;   8 U> IF  -49 THROW  THEN
;   R@ SORDER ! SORDER
;   R@ FOR  CELL+ SWAP OVER !  STEP  DROP
;   R>
;   ENDCASE ;
    head SETORDER,9,"SET-ORDER",docolon,0,0
        defw TRUE,QBRANCH,setor1
        defw MINUSONE,OVER,EQUAL,QBRANCH,setor2,DROP
        defw FORTHWORDLIST,ONE,SETORDER
.setor1 defw EXIT
.setor2 defw ZERO,OVER,EQUAL,QBRANCH,setor4,DROP
        defw ZERO,SORDER,STORE
.setor3 defw EXIT
.setor4 defw DUP,TOR
        defw LIT,8,UGREATER,QBRANCH,setor5
        defw LIT,-49,THROW
.setor5 defw RFETCH,SORDER,STORE,SORDER
        defw RFETCH,TOR
.setor6 defw CELLPLUS,SWOP,OVER,STORE,BRSTEP,setor6
        defw DROP,RFROM
        defw DROP
.setor7 defw EXIT

;X ONLY             --                          Set minimum search order
;   -1 SET-ORDER ;
    head ONLY,4,"ONLY",docolon,0,0
        defw MINUSONE,SETORDER,EXIT

;X ALSO             --                          Duplicate first list in search order
;   GET-ORDER OVER SWAP 1+ SET-ORDER ;
    head ALSO,4,"ALSO",docolon,0,0
        defw GETORDER,OVER,SWOP,ONEPLUS,SETORDER,EXIT

;X PREVIOUS         --                          Remove first list in search order
;   GET-ORDER ?DUP IF  NIP 1- SET-ORDER  THEN ;
    head PREVIOUS,8,"PREVIOUS",docolon,0,0
        defw GETORDER,QDUP,QBRANCH,prev1
        defw NIP,ONEMINUS,SETORDER
.prev1  defw EXIT

;X ORDER            --                          Display search order and compilation wid
;   GET-ORDER ?DUP
;   IF  FOR  CR U.  STEP  THEN
;   GET-CURRENT U. ;
    head ORDER,5,"ORDER",docolon,0,0
        defw GETORDER,QDUP,QBRANCH,order2
        defw TOR
.order1 defw CRR,UDOT,BRSTEP,order1
.order2 defw GETCURRENT,UDOT,EXIT


;Z VOCABULARY       "<spaces>name" --           Create new vocabulary
;   NSDS WORDLIST CREATE ,
;   DOES> @ >R
;         GET-ORDER NIP R> SWAP SET-ORDER ;
    head VOCABULARY,10,"VOCABULARY",docolon,0,0
if WORDSET_REGIONS
        defw NSDS
endif ; WORDSET_REGIONS
        defw WORDLIST,CREATE,COMMA
        defw XDOES
.dovoc  call dodoes
        defw FETCH,TOR
        defw GETORDER,NIP,RFROM,SWOP,SETORDER,EXIT

;Z FORTH            --                          Set first list in search order to FORTH-WORDLIST
;   VOCABULARY FORTH  FORTH-WORDLIST ' FORTH >BODY !
    head FORTH,5,"FORTH",dovoc,0,0
        defw usr_forth


endif ; WORDSET_SEARCHORDER
