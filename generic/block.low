; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

if WORDSET_BLOCK

; ======================================================================
; CamelForth: ANS Block (& Extension) wordsets (low-level words)
; ======================================================================


;X EMPTY-BUFFERS    --                          Unassign all buffers
;   'BUFFER @ ?DUP
;   IF  0 SWAP  4 FOR  2DUP C! SWAP 1+ SWAP 1+  STEP  NIP
;       8 ERASE
;   THEN ;
    head EMPTYBUFFERS,13,"EMPTY-BUFFERS",docode,0,0
        ld hl,(usr_tickbuffer)
        ld a,h
        or l
        jp z,next_code                  ; do nothing if no structure
        push bc
        ld bc,$0400                     ; B=4, C=0
.blkin1 ld (hl),c                       ; set MRU list to 0,1,2,3
        inc hl
        inc c
        djnz blkin1
        ld bc,$0800                     ; B=8, C=0
.blkin2 ld (hl),c                       ; erase list of block numbers
        inc hl
        djnz blkin2
        pop bc
        next


;- SAVEBUF          n --                        Save block in buffer n if necessary
    nohead SAVEBUF,docode
        ld hl,(usr_tickbuffer)
        ld a,c
        add hl,bc
        add hl,bc
        ld c,5
        add hl,bc                       ; HL points to high byte of block num
        bit 7,(hl)
        jr z,svbuf2                     ; do nothing if not updated
        res 7,(hl)
        ld b,(hl)
        dec hl
        ld c,(hl)                       ; BC=block number
        exx
        ld hl,(usr_tickbuffer)
        ld bc,12
        add hl,bc                       ; HL points to buffer 0
        ld bc,1024
        and a
        jr z,svbuf1
.svbflp add hl,bc
        dec a
        jr nz,svbflp
.svbuf1 push hl                         ; stack address of buffer to save
        push bc                         ; stack 1024
        exx
        call docolon
        defw BLOCKSEEK,WRITEFILE,THROW,EXIT
.svbuf2 pop bc                          ; new TOS
        next

;- BLOCKS?                                      THROW an exception if no blocks
;   'BUFFER @ 0=
;   IF  -35 THROW  THEN ;
    nohead BLOCKSQ,docode
        ld hl,(usr_tickbuffer)
        ld a,h
        or l
        jp nz,next_code                 ; okay if 'BUFFER<>0
.thro35 ld bc,-35
        jp THROW                        ; else -35 THROW

;- FINDBUF          u -- caddr flag             Find buffer for block: TRUE if loaded
;                                       Updates MRU list. Does BLOCKS? itself
    nohead FINDBUF,docode
        push bc
        exx                             ; use alternate set
        pop bc
        ld hl,(usr_tickbuffer)
        ld a,h
        or l
        jr z,thro35                     ; error if no blocks
        dec bc
        ld a,b
        inc bc
        and $c0
        jr nz,thro35                    ; or if block number >16384
        inc hl
        inc hl
        inc hl
        inc hl                          ; HL=address of block list
        ld a,4                          ; A=4-current buf#
.fbuf1  ld e,(hl)
        inc hl
        ld d,(hl)
        inc hl
        res 7,d
        ex de,hl
        and a
        sbc hl,bc
        ex de,hl
        jr z,fbuf2                      ; move on if found block in a buffer
        dec a
        jr nz,fbuf1                     ; back to search others
        ld a,(usr_numbufs)
        dec a                           ; A=offset of LRU buffer
        push bc                         ; save block number
        ld c,a
        ld b,0
        ld hl,(usr_tickbuffer)
        push hl
        add hl,bc
        ld c,(hl)                       ; BC=LRU buffer number
        pop hl
        inc hl
        inc hl
        inc hl
        inc hl
        add hl,bc
        add hl,bc                       ; HL=address to store block #
        push hl                         ; save it
        push bc
        call setmru                     ; set it as MRU buffer, get addr in HL
        pop bc
        push hl                         ; stack caddr
        ld hl,0
        push hl                         ; stack FALSE
        push bc
        exx                             ; back to normal set
        pop bc                          ; TOS=LRU buffer number again
        call docolon
        defw SAVEBUF,TWOSWAP,STORE,EXIT ; save buffer, set block #, exit
.fbuf2  ld e,a
        ld a,4
        sub e
        ld c,a                          ; C=buffer number
        call setmru                     ; set it as MRU buffer, get addr in HL
        push hl                         ; stack caddr
        exx                             ; back to normal set
        ld bc,-1                        ; TOS=TRUE, found in buffer
        next

; SetMRU subroutine
; On entry, C=buffer number (0..3)
; On exit, HL=address of buffer

.setmru ld de,(usr_tickbuffer)          ; DE=address of MRU list
        ld hl,12-1024
        add hl,de                       ; HL=address of buffer 0 - 1024
        ex de,hl
        ld b,c
.setmr2 ld a,(hl)
        ld (hl),b                       ; move entries down list
        inc hl
        ld b,a
        cp c
        jr nz,setmr2                    ; until position of ours filled
        ex de,hl
        ld de,1024
        inc c
.setmr3 add hl,de
        dec c
        jr nz,setmr3                    ; generate buffer address
        ret

;X UPDATE           --                          Mark block buffer as updated
;   BLOCKS?
;   'BUFFER @ DUP C@ 2* + 4 +
;   DUP @ 32768 OR SWAP ! ;  
    head UPDATE,6,"UPDATE",docode,0,0
        ld hl,(usr_tickbuffer)
        ld a,h
        or l
        jp z,thro35                     ; do the -35 THROW if no 'BUFFER
        push bc
        ld a,(hl)                       ; A=MRU buffer number
        add a,a
        ld c,a
        ld b,0
        add hl,bc
        ld c,5
        add hl,bc                       ; HL points to high byte of block number
        pop bc
        set 7,(hl)                      ; set update flag
        next


endif ; WORDSET_BLOCK
