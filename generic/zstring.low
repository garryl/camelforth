; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

if WORDSET_ZSTRING

; ======================================================================
; CamelForth: Null-terminated string handling (low-level words)
; ======================================================================


;Z S>0              c-addr n -- c-addr'         Convert string to null-terminated
    head STRTONUL,3,"S>0",docode,0,strnun-strnuf
.strnuf pop hl          ; HL=source address
        ld b,0          ; ensure count < 256 to avoid memory corruption
        push de         ; save IP
        ld de,usr_zpad
        ld a,c
        and a
        jr z,STRTONUL2  ; skip copy if zero length
        ldir            ; copy
.STRTONUL2
        ld a,c
        ld (de),a       ; add nul
        pop de
        ld bc,usr_zpad  ; TOS=address of null-terminated string (0PAD)
.strnun
        next


endif ; WORDSET_ZSTRING
