\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading multiprogramming demo...)
\ Demo of multiprogramming facilities, with 3 tasks running
\ in separate windows, and CamelForth available in another.

\ Three tasks

DECIMAL
TASK: A  TASK: B  TASK: C

\ Some actions for the tasks

: T_A  0 BEGIN  DUP [CHAR] 0 + EMIT 1+ DUP 9 > IF  0=  THEN PAUSE  AGAIN ;
: T_B  1 BEGIN  CR DUP U. 2* ?DUP 0= IF  1  THEN PAUSE  AGAIN ;
: T_C  RAND BEGIN  16 RND 8 * ATTRIB PAGE PAUSE  AGAIN ;

\ Define a window and set up each task; they will use the active window
\ at the time that TASK! is executed

CINIT

1 WINDOW  31 ATTRIB
5 20 20 10 S" Task A" OPENPOPUP
' T_A A TASK!

2 WINDOW  30 ATTRIB
30 20 20 10 S" Task B" OPENPOPUP
' T_B B TASK!

3 WINDOW  23 ATTRIB
55 20 20 10 S" Task C" OPENPOPUP
' T_C C TASK!

\ Set up the window for the main terminal task

0 WINDOW  31 ATTRIB
0 0 80 16 S"  Sprinter CamelForth - Multitasking Demo " OPENTITLED

\ Start the tasks running

MULTI  A WAKE  B WAKE  C WAKE

\ Tell the user what's going on

23 ATTRIB
.( To stop task A etc, type:  A SLEEP) CR
.( To restart task A etc, type:  A WAKE) CR
.( To disable multitasking, type:  SINGLE) CR
.( To enable multitasking, type: MULTI) CR
.( You can execute any FORTH words you like in this window.) CR
.( Type WORDS to see current vocabulary.) CR
.( Type BYE to exit Sprinter CamelForth.) CR

