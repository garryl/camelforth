\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

CR .( Loading HEX-to-DECIMAL converter...)
\ An example of using CamelForth to create EXE commands

: -LEADING ( c u -- c' u' )
   BEGIN  OVER C@ BL = OVER 0<> AND  WHILE  1 /STRING  REPEAT ;

: hexdec. ( c u -- c' u' )
   -LEADING 0 0 2SWAP HEX >NUMBER 2SWAP DROP DUP U. ." = " DECIMAL U. CR ;

: hexstring ( c u -- )
   hexdec. BEGIN  DUP  WHILE  1 /STRING hexdec.  REPEAT 2DROP ;

: hexhelp
   ." Type HEXTODEC followed by a list of hex numbers" CR ;

: hextodec
   CMDLINE COUNT -TRAILING -LEADING
   DUP IF  hexstring  ELSE  2DROP hexhelp  THEN BYE ;

\ Make our top-level word the first thing Forth executes
' hextodec IS (COLD)

\ Create the EXE file
S" hextodec.exe" SAVE-EXE

CR .( HEXTODEC.EXE has been created)

