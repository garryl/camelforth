\ ****************************************************************************
\ CamelForth for the Zilog Z80
\ Copyright (c) 1994,1995 Bradford J. Rodriguez
\ With contributions by Douglas Beattie Jr., 1998
\ Widely extended and reorganised by Garry Lancaster, 1999-2011
\ Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
\
\ This program is free software; you can redistribute it and/or modify
\ it under the terms of the GNU General Public License as published by
\ the Free Software Foundation; either version 3 of the License, or
\ (at your option) any later version.
\
\ This program is distributed in the hope that it will be useful,
\ but WITHOUT ANY WARRANTY; without even the implied warranty of
\ MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
\ GNU General Public License for more details.
\
\ You should have received a copy of the GNU General Public License
\ along with this program.  If not, see <http://www.gnu.org/licenses/>.
\ ****************************************************************************

\ LIBMAN for CamelForth
CR .( Loading LIBMAN...)

\ Ensure HERE=$4000, as that's what the binary was assembled to
S" LIBMAN must be loaded first!" ABORT"S 2!
HERE 16384 <> -2 AND THROW
HERE

\ Load the binary
CR .( Loading LIBMAN.BIN...)
S" libman.bin" R/O OPEN-FILE THROW TUCK
DUP FILE-SIZE THROW DROP DUP ALLOT
SWAP READ-FILE THROW DROP
CLOSE-FILE THROW


