# ****************************************************************************
# CamelForth for the Zilog Z80
# Copyright (c) 1994,1995 Bradford J. Rodriguez
# With contributions by Douglas Beattie Jr., 1998
# Widely extended and reorganised by Garry Lancaster, 1999-2011
# Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ****************************************************************************

#
# Build ZX Spectrum +3/+3e CamelForth
#

# This script generates a binary file (camel.bin) which can be loaded to
# memory at 25280, after performing a CLEAR 23999. Run CamelForth by
# entering RANDOMIZE USR 25280.
# A glossary (glossary.txt) is also automatically generated.

cp ../generic/* .
z80asm -b -nl -ns -nm -DBUILD_PRJFILE -ocamel.prj options.asm
gforth preprocess.fth
z80asm -b -l -ns -nm -ocamel.bin camel.asm

# Generate glossary
grep ';[[:upper:]]' camel.lst | cut --delimiter=";" -f2- | cut -c3- | LC_ALL=C sort > glossary.txt

# If you have installed John Elliott's Taptools utilities (see
# http://www.seasip.demon.co.uk/ZX/unix.html) then the following lines
# will automatically generate a camel.zxb file and a camel.tap file.
cp camel.bin camel.zxb
specform -a 25280 camel.zxb
mv camel.zxb.zxb camel.zxb
tapcat -N camel.tap camel.bas camel.zxb

# You can then either:
# 1. Copy camel.bas and camel.zxb to a +3/+3e disk, and load with:
#    LOAD "camel.bas"
# or
# 2. Use the camel.tap file with an emulator (or use it to generate a
#    real cassette tape to load into a Spectrum).

