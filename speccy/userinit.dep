; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2014
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2014
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: User Variables: Initial values (system-dependent)
; System: ZX Spectrum (generic 48K+)
; ======================================================================


; Generally, this file will not need to be altered. However, on some
; systems it may be useful to alter some of the default vectors. For
; example, on the Spectrum the default vector for (KEY) and (ACC_EVT)
; is CAPS, which toggles caps lock on as required (since the OS doesn't
; do this automatically).


.stduinit
        defw -1         ; PENDING-CHAR
        defw CAMEL      ; (COLD) vector
        defw CAPS       ; (KEY) vector
        defw CAPS       ; (ACC_EVT) vector
        defw NOOP       ; (ACC_MAIL) vector
        defw CEMIT      ; EMIT
        defw CKEY       ; KEY
        defw CTYPE      ; TYPE
        defw EKEYS      ; EKEY
        defw BRDOTERR   ; .ERROR
        defw lastword   ; LATEST
        defw lastenv    ; E0
        defw usr_dps+0  ; CURDS (RAM)
        defw region0_dp ; DPS 0==RAM
if WORDSET_REGIONS
        defw region1_dp ;     1==ROM2
        defw region2_dp ;     2==ROM1
        defw usr_dps+0  ; CURNS (RAM)
else
        defs 6
endif
if WORDSET_SEARCHORDER
        defw usr_wids   ; NEXTLIST
        defw lastword   ; FORTH-WORDLIST
        defw 0,0,0,0    ; 8 new wordlists
        defw 0,0,0,0
        defw usr_forth  ; CURRENT
        defw 1          ; SORDER (9 cells)
        defw usr_forth
        defw 0,0,0,0,0,0,0
else
        defs 40
endif
if WORDSET_MULTI
        defw NOOP       ; PAUSE
        defw BRRUN      ; RUN
else ; WORDSET_MULTI
        defs 4
endif ; WORDSET_MULTI
if WORDSET_BLOCK
        defw 0          ; 'BUFFER
        defb 0          ; #BUFS
else ; WORDSET_BLOCK
        defs 3
endif ; WORDSET_BLOCK

        include "initsgbl.dep"          ; system-dependent global initialisation

; These are the per-task initial values

if WORDSET_MULTI
        defw pertask_start              ; LINK
else
        defs 2
endif
        defw 10                         ; BASE
        defw 0                          ; S0
        defw usr_rstacktop              ; R0

        include "initspt.dep"           ; system-dependent per-task initialisation


