; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: Character Output code fragment (system-dependent)
; System: ZX Spectrum (generic 48K+)
; ======================================================================


; This file contains a code fragment which outputs the character
; in A to the standard output, and is used in the file "io.dep"
; It must preserve BC/DE/HL/IX/IY.

; As this file will be "include"d multiple times, no labels can be
; used; if this is necessary, it should simply be a "call" to a routine
; defined elsewhere ("startup.dep" is a good place).


; ZX Spectrum version
; The zx_charout routine is defined in startup.dep

        call    zx_charout

