; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth Extras - Sprinter version
; ======================================================================

; This file defines extra modules for the Sprinter version of CamelForth.
; WORDSET_ZSTRING is required for standard ANS file wordset on the Sprinter.


define  SPRINTER_WINDOWS
define  SPRINTER_GRAPHICS
define  SPRINTER_DIRS

; ======================================================================
; DO NOT MODIFY BEYOND HERE
; ======================================================================

; A project file is now built based on the options selected

if BUILD_PRJFILE


if SPRINTER_WINDOWS
        defm    "windows.spr"&$0d&$0a
endif
if SPRINTER_GRAPHICS
        defm    "graphics.spr"&$0d&$0a
endif
if SPRINTER_DIRS
        defm    "dirs.spr"&$0d&$0a
endif
        defm    "exeend.spr"&$0d&$0a


endif ; BUILD_PRJFILE
