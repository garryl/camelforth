; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: Directory handling (Sprinter-specific)
; ======================================================================


;Z 0>S              0-addr -- c u               Convert null-terminated to string
    head ZEROTOS,3,"0>S",docode,0,0
        push    bc
        ld      h,b
        ld      l,c
        ld      bc,-1
.ztos2  ld      a,(hl)
        inc     hl
        inc     bc
        and     a
        jr      nz,ztos2
        next

;Z CURDISK          -- n ior                    Get current disk number (0=A, 1=B...)
    head CURDISK,7,"CURDISK",docode,0,0
        push    bc
        push    iy
        push    ix
        push    de
        ld      c,DSS_CURDISK
        rst     DSS
        ld      h,0
        ld      l,a
        call    setior
        pop     de
        pop     ix
        pop     iy
        push    hl
        next

;Z CHDISK           n -- ior                    Set current disk number (0=A, 1=B...)
    head CHDISK,6,"CHDISK",docode,0,0
        push    iy
        push    ix
        push    de
        ld      a,c
        ld      c,DSS_CHDISK
        rst     DSS
        call    setior
        pop     de
        pop     ix
        pop     iy
        next

;Z CURDIR           a -- ior                    Get current directory into 256-byte buffer
    head CURDIR,6,"CURDIR",docode,0,0
        push    iy
        push    ix
        push    de
        ld      h,b
        ld      l,c
        ld      c,DSS_CURDIR
        rst     DSS
        call    setior
        pop     de
        pop     ix
        pop     iy
        next

;Z CHDIR            a -- ior                    Set current directory from null-term string
    head CHDIR,5,"CHDIR",docode,0,0
        push    iy
        push    ix
        push    de
        ld      h,b
        ld      l,c
        ld      c,DSS_CHDIR
        rst     DSS
        call    setior
        pop     de
        pop     ix
        pop     iy
        next

;Z MKDIR            a -- ior                    Create directory from null-term string
    head MKDIR,5,"MKDIR",docode,0,0
        push    iy
        push    ix
        push    de
        ld      h,b
        ld      l,c
        ld      c,DSS_MKDIR
        rst     DSS
        call    setior
        pop     de
        pop     ix
        pop     iy
        next

;Z RMDIR            a -- ior                    Remove directory from null-term string
    head RMDIR,5,"RMDIR",docode,0,0
        push    iy
        push    ix
        push    de
        ld      h,b
        ld      l,c
        ld      c,DSS_RMDIR
        rst     DSS
        call    setior
        pop     de
        pop     ix
        pop     iy
        next

