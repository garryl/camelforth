; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2011
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2011
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth: EXE header (Sprinter-specific)
; ======================================================================

; This fragment is included twice: once for the EXE header itself,
; and once for a copy used by SAVE-EXE

        defm    "EXE"                           ; EXE ID
        defb    $00                             ; EXE version
        defw    loadaddr-exeheader              ; code offset low
        defw    $0000                           ; code offset high
if SPRINTER_GRAPHICS
        defw    gsubsysst-loadaddr
else
        defw    exeend-loadaddr                 ; end-beg; primary loader
endif ; SPRINTER_GRAPHICS
        defw    0,0,0                           ; 3 reserved words
        defw    $8100                           ; load address
        defw    binary_start+$8100-$c100        ; start address
        defw    $bfee                           ; stack address
