; ****************************************************************************
; CamelForth for the Zilog Z80
; Copyright (c) 1994,1995 Bradford J. Rodriguez
; With contributions by Douglas Beattie Jr., 1998
; Widely extended and reorganised by Garry Lancaster, 1999-2014
; Z88, Sprinter, ZX Spectrum +3/+3e ports by Garry Lancaster, 1999-2014
;
; This program is free software; you can redistribute it and/or modify
; it under the terms of the GNU General Public License as published by
; the Free Software Foundation; either version 3 of the License, or
; (at your option) any later version.
;
; This program is distributed in the hope that it will be useful,
; but WITHOUT ANY WARRANTY; without even the implied warranty of
; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
; GNU General Public License for more details.
;
; You should have received a copy of the GNU General Public License
; along with this program.  If not, see <http://www.gnu.org/licenses/>.
; ****************************************************************************

; ======================================================================
; CamelForth Options
; ======================================================================

; Use this file to define wordsets and other features to be built into
; CamelForth. To remove a wordset from the build, simply comment out
; the DEFINE with a semi-colon.


; ======================================================================
; ANS standard wordsets
; ======================================================================

define  WORDSET_FACILITY
define  WORDSET_SEARCHORDER
define  WORDSET_STRING
define  WORDSET_TOOLS
define  WORDSET_FILE            ; Required by BLOCK fileset
define  WORDSET_BLOCK           ; ANS requires this if FILE wordset used


; ======================================================================
; Non-ANS standard features
; ======================================================================

define  WORDSET_FASTCODE        ; inline code compilation
define  WORDSET_MULTI           ; multiprogramming
define  BANKSWITCHED            ; safer taskswitching for banked systems
define  WORDSET_ZSTRING         ; null-terminated string handling
define  WORDSET_TRIG            ; integer trigonometric functions
define  WORDSET_RANDOM          ; random number generation
;define WORDSET_REGIONS         ; code/data regions


; ======================================================================
; Further extras
; ======================================================================

;  Define this if you are providing an "extras.asm" options file (see
;  the Z88 one for an example)

define EXTRAS_FILE


; ======================================================================
; DO NOT MODIFY BEYOND HERE
; ======================================================================

; A project file is now built based on the options selected

if BUILD_PRJFILE

org 0
module cfproject

        defm    "options.asm"&$0d&$0a
        defm    "startup.dep"&$0d&$0a
        defm    "userinit.dep"&$0d&$0a
        defm    "compile.hi"&$0d&$0a
        defm    "compile.low"&$0d&$0a
        defm    "defining.hi"&$0d&$0a
        defm    "defining.low"&$0d&$0a
        defm    "except.hi"&$0d&$0a
        defm    "io.hi"&$0d&$0a
        defm    "io.low"&$0d&$0a
        defm    "io.dep"&$0d&$0a
        defm    "maths.hi"&$0d&$0a
        defm    "maths.low"&$0d&$0a
        defm    "memory.hi"&$0d&$0a
        defm    "memory.low"&$0d&$0a
        defm    "misc.hi"&$0d&$0a
        defm    "parser.hi"&$0d&$0a
        defm    "parser.low"&$0d&$0a
        defm    "parser.dep"&$0d&$0a
        defm    "stack.low"&$0d&$0a
        defm    "stack.hi"&$0d&$0a
        defm    "structs.hi"&$0d&$0a
        defm    "structs.low"&$0d&$0a
        defm    "vars.hi"&$0d&$0a
        defm    "vars.low"&$0d&$0a
if WORDSET_FACILITY
        defm    "facility.dep"&$0d&$0a
endif
if WORDSET_SEARCHORDER
        defm    "search.hi"&$0d&$0a
        defm    "search.low"&$0d&$0a
endif
if WORDSET_STRING
        defm    "string.hi"&$0d&$0a
        defm    "string.low"&$0d&$0a
endif
if WORDSET_TOOLS
        defm    "tools.hi"&$0d&$0a
endif
if WORDSET_FILE
        defm    "file.hi"&$0d&$0a
        defm    "file.dep"&$0d&$0a
endif
if WORDSET_BLOCK
        defm    "block.hi"&$0d&$0a
        defm    "block.low"&$0d&$0a
endif
if WORDSET_FASTCODE
        defm    "fastcode.hi"&$0d&$0a
        defm    "fastcode.low"&$0d&$0a
endif
if WORDSET_MULTI
        defm    "multi.hi"&$0d&$0a
        defm    "multi.low"&$0d&$0a
endif
if WORDSET_ZSTRING
        defm    "zstring.hi"&$0d&$0a
        defm    "zstring.low"&$0d&$0a
        defm    "zstring.dep"&$0d&$0a
endif
if WORDSET_TRIG
        defm    "trig.hi"&$0d&$0a
endif
if WORDSET_RANDOM
        defm    "random.hi"&$0d&$0a
        defm    "random.dep"&$0d&$0a
endif
if WORDSET_REGIONS
        defm    "regions.low"&$0d&$0a
endif

endif ; BUILD_PRJFILE


if EXTRAS_FILE
        include "extras.asm"
endif

if BUILD_PRJFILE
        defm    "finishup.low"&$0d&$0a
endif

