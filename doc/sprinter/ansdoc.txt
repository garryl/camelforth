Sprinter CamelForth - ANS Forth Compliance Documentation
========================================================

Sprinter CamelForth is an ANS Forth Standard System:

  * providing  .( .R 0<> 0> 2>R 2R> 2R@ :NONAME <> ?DO AGAIN CASE COMPILE,
               ENDCASE ENDOF ERASE FALSE HEX NIP OF PAD PARSE REFILL
	       RESTORE-INPUT SAVE-INPUT SOURCE-ID TIB TO TRUE TUCK U.R U>
               UNUSED VALUE WITHIN \
    from the Core Extensions word set
  * providing the Block word set
  * providing the Block Extensions word set
  * providing  D+ D- DABS DNEGATE M+  from the Double-Number word set
  * providing  DU<  from the Double-Number Extensions word set
  * providing the Exception word set
  * providing the Exception Extensions word set
  * providing the Facility word set
  * providing the Facility Extensions word set
  * providing the File-Access word set
  * providing the File-Access Extensions word set
  * providing  .S ? WORDS  from the Programming Tools word set
  * providing  BYE CODE DUMP EDITOR
    from the Programming Tools Extensions word set
  * providing the Search-Order word set
  * providing the Search-Order Extensions word set
  * providing the String word set
  * providing the String Extensions word set


Optionally-loaded components
============================
A few of the words mentioned or implied above are not present in the system
as initially started, since there was not enough space to include everything
in the 16K segment used for CamelForth. These can be loaded at any point
using the additional source files provided with the system, with the phrase:
  S" filename.fth" INCLUDED

* Environmental Queries: ENVIRONMENT.FTH
  --------------------------------------
  Environmental queries will always produce FALSE on the standard system,
  until this file is loaded which provides the strings required for the
  queries to provide meaningful values.

* Line Editor: LINEEDIT.FTH
  -------------------------
  The optional line editor provides:
    LIST  SCR	from the Block Extensions word set
    EDITOR	from the Programming Tools Extensions word set
  It contains all the commands of the classic fig-style line editor, with
  several additional features.

* Programming Tools: DUMP.FTH
  ---------------------------
  This file provides:
    DUMP	from the Programming Tools Extensions word set

* Enhanced error reporting: ERRORS.FTH
  ------------------------------------
  The standard system produces appropriate messages for exceptions
  -1, -2 and -13, but reports all other exceptions as a numeric value.
  When loaded, this file provides error messages for all CamelForth-
  generated exceptions, including identification of the word causing
  exception -4095.


Implementation-defined options: CORE and CORE EXTENSION word sets
=================================================================

* Aligned address requirements
  ----------------------------
  There are no special aligned address requirements, and consequently all
  addresses are treated as aligned.

* Behaviour of EMIT for non-graphics characters
  ---------------------------------------------
  The following non-graphics characters are recognised by EMIT:
      8  Move cursor left
      9  Move cursor right
     12  Clear window
     13  Carriage return
  All other characters produce graphic symbols in the standard Sprinter
  character set.

* Character editing of ACCEPT
  ---------------------------
        [BACKSPACE]     Delete character to left of cursor
	[LEFT]		Move cursor left
	[RIGHT]		Move cursor right
        [HOME]          Move cursor to start of line
        [END]           Move cursor to end of line
        [CAPS LOCK]     Switch caps lock mode
        [RETURN]        Terminate input
  These editing facilities can be extended by handling additional codes
  with the deferred word (ACC_EVT).

* Character set
  -------------
  The standard Sprinter 8-bit character set in use by DSS

* Character-aligned address requirements
  --------------------------------------
  There are no special character-aligned address requirements, and
  consequently all character addresses are treated as aligned.

* Character-set-extensions matching characteristics
  -------------------------------------------------
  Any characters except control characters may be used in a definition name.
  Matching is case-sensitive.

* Conditions under which control characters match a space delimiter
  -----------------------------------------------------------------
  Control characters match space delimiters if a space delimiter is used
  with WORD or PARSE. As the text interpreter uses WORD, control characters
  also match space delimiters during parsing of any input source.

* Control-flow stack format
  -------------------------
  The control-flow stack is not seperate, but implemented using the data stack.

* Conversion of digits larger than 35
  -----------------------------------
  Digits larger than 35 are represented by ASCII characters 91 decimal and
  above in strict numerical order, so for digits 36 to 72, the following
  characters are used:
    [\]^_`abcdefghijklmnopqrstuvwxyz{|}~
  ASCII code 127 cannot be entered from the keyboard, so the highest usable
  base is 73. Also note that lower-case characters always represent digits
  42 to 67, and never digits 10 to 35.

* Display after input terminates in ACCEPT
  ----------------------------------------
  On termination of accept, input remains on the screen as entered with the
  cursor position at the following character.

* Exception abort sequence of ABORT"
  ----------------------------------
  The address and count of the error string is stored in the double-cell user
  variable ABORT"S and exception -2 is THROWn.

* Input line terminator
  ---------------------
  ASCII 13 (CR) is used to terminate input, and is produced by pressing RETURN
  or ENTER.

* Maximum size of a counted string
  --------------------------------
  255 characters

* Maximum size of a parsed string
  -------------------------------
  255 characters

* Maximum size of a definition name
  ---------------------------------
  127 characters

* Maximum string length for ENVIRONMENT?
  --------------------------------------
  255 characters

* Method of selecting user input device
  -------------------------------------
  The user input device can be altered by revectoring the word KEY to a
  user-defined word.

* Method of selecting user output device
  --------------------------------------
  The user output device can be altered by revectoring the words EMIT and
  TYPE to user-defined words.

* Methods of dictionary compilation
  ---------------------------------
  Definitions are compiled as a single contigious area, and there is no
  distinction between namespace, codespace and dataspace.

* Number of bits in one address unit
  ----------------------------------
  8 bits

* Number representation and arithmetic
  ------------------------------------
  Binary two's-complement

* Ranges for n, +n, u, d, +d, ud
  ------------------------------
  n	{ -32768..+32767 }
  +n	{ 0..32767 }
  u	{ 0..65535 }
  d	{ -2147483648..+2147483647 }
  +d	{ 0..2147483647 }
  ud	{ 0..4294967295 }

* Read-only data space regions
  ----------------------------
  Writeable data space is located from $4000-$bffa. All other regions (except
  areas whose addresses are provided by the system, such as PAD)
  are read-only.

* Size of buffer at WORD
  ----------------------
  This is equivalent to the total unused space in the current region, and is
  therefore the value returned by UNUSED in address units.

* Size of one cell in address units
  ---------------------------------
  2 address units

* Size of one character in address units
  --------------------------------------
  1 address unit

* Size of keyboard terminal input buffer
  --------------------------------------
  80 characters

* Size of pictured numeric output string buffer
  ---------------------------------------------
  36 characters

* Size of scratch area whose address is returned by PAD
  -----------------------------------------------------
  84 characters

* System case-sensitivity characteristics
  ---------------------------------------
  The system is case-sensitive for dictionary searches, string comparisons
  and numeric input.
  Filenames are not case-sensitive, and follow the normal Sprinter rules.

* System prompt
  -------------
  Lowercase " ok" during interpretation. No prompt during compilation.

* Type of division rounding
  -------------------------
  Floored

* Values of STATE when true
  -------------------------
  STATE may have the following values:
   0		interpretation mode
   1		standard compilation mode
  Words using STATE and acting on it to compile items into the dictionary
  should be aware that in compilation mode, different actions should be
  taken if "fast code" compilation is on (this can be determined with FAST?
  which returns 0 for normal compilation mode and 1 for fast code mode).
  The standard words COMPILE, and LITERAL take account of this mode, and so
  words using these to perform their compilation actions will be fine.

* Values returned after arithmetic overflow
  -----------------------------------------
  Arithmetic is performed in two's-complement, and thus modulo 2^16 for single
  precision arithmetic, and modulo 2^32 for double precision arithmetic.
  Division by zero returns a value of -2.

* Whether the current definition can be found after DOES>
  -------------------------------------------------------
  No


Ambiguous conditions: CORE and CORE EXTENSION word sets
=======================================================

* A name is neither a valid definition name or number during interpretation
  -------------------------------------------------------------------------
  The address and count of the undefined name is stored in the double-cell user
  variable ABORT"S and then exception -13 is THROWn.

* A definition name exceeded the maximum length allowed
  -----------------------------------------------------
  Ignore and continue: the definition will be incorrectly entered into the
  dictionary and will cause a crash if execution is attempted.

* Addressing a region not listed as dataspace
  -------------------------------------------
  Ignore and continue: a crash may occur if system information is overwritten.

* Argument type incompatible with specified input parameter
  ---------------------------------------------------------
  Ignore and continue: unexpected results will occur, including possible
  exceptions from operating system routines.

* Attempting to obtain the execution token of a definition with undefined
  interpretation semantics
  -----------------------------------------------------------------------
  Ignore and continue: the xt may be successfully compiled with COMPILE, but
  attempts to execute it will cause unexpected results including possible
  crashes.

* Dividing by zero
  ----------------
  Ignore and continue: an erroneous result will be returned.

* Insufficient data-stack space or return-stack space (stack overflow)
  --------------------------------------------------------------------
  Ignore and continue: a crash may occur.

* Insufficient space for loop-control parameters
  ----------------------------------------------
  Ignore and continue: a crash may occur.

* Insufficient space in the dictionary
  ------------------------------------
  Ignore and continue: if following regions are in ROM, compilation will have
  no further effect; if in RAM, other information will be overwritten, possibly
  causing a crash.

* Interpreting a word with undefined interpretation semantics
  -----------------------------------------------------------
  Ignore and continue: unexpected results will occur, including a possible
  crash.

* Modifying the results of the input buffer or a string literal
  -------------------------------------------------------------
  Ignore and continue: modification will be successful unless the string
  literal is in ROM.

* Overflow of a pictured numeric output string
  --------------------------------------------
  Ignore and continue: the file line input area will be overwritten which
  may cause problems if interpretation of a file is in progress.

* Parsed string overflow
  ----------------------
  Ignore and continue: no overflow can occur with PARSE, but WORD may overflow
  with the same result as overflowing the dictionary.

* Producing a result out of range
  -------------------------------
  Ignore and continue: results will be returned modulo 2^16 for single
  precision arithmetic and modulo 2^32 for double precision arithmetic.

* Reading from an empty data stack or return stack (stack underflow)
  ------------------------------------------------------------------
  Ignore and continue: a crash may occur.

* Unexpected end of input buffer, resulting in attempt to use zero-length
  string as a name
  -----------------------------------------------------------------------
  Ignore and continue: the zero-length name will be entered in the dictionary
  but will not be findable.

* >IN greater than size of input buffer
  -------------------------------------
  Ignore and continue: parsing will erroneously continue beyond the end of the
  input buffer.

* RECURSE appears after DOES>
  ---------------------------
  The execution token of the defining word currently being defined will be
  compiled into the action of the words it defines.

* Argument input source different than current input source for RESTORE-INPUT
  ---------------------------------------------------------------------------
  The input source is correctly restored.

* Data space containing definitions is de-allocated
  -------------------------------------------------
  This is not possible, so no ambiguous condition exists.

* Data space read/write with incorrect alignment
  ----------------------------------------------
  All alignments are valid, so no ambiguous condition exists.

* Data-space pointer not properly aligned
  ---------------------------------------
  All alignments are valid, so no ambiguous condition exists.

* Less than u+2 stack items for PICK or ROLL
  ------------------------------------------
  Neither PICK or ROLL are implemented.

* Loop-control parameters not available
  -------------------------------------
  Ignore and continue: a crash may occur.

* Most recent definition does not have a name (IMMEDIATE)
  -------------------------------------------------------
  The most recent named definition is used instead.

* Name not defined by VALUE used by TO
  ------------------------------------
  Ignore and continue: the definition may be corrupted.

* Name not found for ' POSTPONE [']
  ---------------------------------
  The address and count of the undefined name is stored in the double-cell user
  variable ABORT"S and then exception -13 is THROWn.

* Parameters are not of the same type for DO ?DO WITHIN
  -----------------------------------------------------
  Ignore and continue: unexpected results will occur.

* POSTPONE applied to TO
  ----------------------
  The semantics of TO will be compiled into the current definition. On
  execution, according to the value of STATE either the interpretation or
  compilation semantics of TO will be performed.

* String longer than a counted string returned by WORD
  ----------------------------------------------------
  The length of the string modulo 256 will be returned in the count.

* u greater than or equal to number of bits in a cell for LSHIFT or RSHIFT
  ------------------------------------------------------------------------
  u is taken modulo 256. If the value is still greater or equal to the
  number of bits in a cell, then the result will be zero.

* Word not defined by CREATE for >BODY >DOES
  ------------------------------------------
  Ignore and continue: unexpected results may occur, although >BODY returns
  useful results for VARIABLEs VALUEs and DEFERred words.

* Words improperly used outside <# and #>
  ---------------------------------------
  Ignore and continue: unexpected results will occur, including corruption of
  the top two stack itmes, possibly resulting in stack underflow and a crash.


Other system documentation: CORE and CORE EXTENSION word sets
=============================================================

* Non-standard words using PAD
  ----------------------------
  None. Note that nesting of input levels above 6 will overwrite PAD, however.

* Operator's terminal facilities
  ------------------------------
  The standard terminal provides a 32 line by 80 character scrolling display
  with input facilities described above as for ACCEPT.
  Enhanced terminal facilities such as multiple windows are documented
  elsewhere.

* Program dataspace available in address units
  --------------------------------------------
  The space available in the current region can be displayed with UNUSED U.

* Return stack space available in cells
  -------------------------------------
  The main terminal task has 93 cells of return stack space; other tasks each
  have 64 cells of return stack space.

* Stack space available in cells
  ------------------------------
  A total of 128 cells is available for the data stack (in practice, more
  is normally available to the active task, but a maximum of 128 cells
  must be present when PAUSE is executed).

* System dictionary space required in address units
  -------------------------------------------------
  CamelForth uses the whole of segment 3 for its system dictionary (16K
  address units).


Implementation-defined options: BLOCK and BLOCK EXTENSION word sets
===================================================================

* Format used for display by LIST
  -------------------------------
  A header showing the "Block " and the block number is first displayed,
  followed by 16 lines of 64 characters preceded by the line number (0..15)
  and a space.

* Length of a line affected by \
  ------------------------------
  64 characters


Ambiguous conditions: BLOCK and BLOCK EXTENSION word sets
=========================================================

* Correct block read was not possible
  -----------------------------------
  Exception -35 (invalid block number) is THROWn if the block number is not
  in the range 1..16384. 
  If the relevant range of blocks has not been mapped to a file with MAP,
  the error will be ignored.
  Otherwise, block reads of non-existent blocks are usually successful,
  resulting in the file being extended as necessary. However, if
  an ior result is returned by the file handling words, block words will
  THROW the ior as an exception.

* I/O exception in block transfer
  -------------------------------
  See above discussion.

* Invalid block number
  --------------------
  Exception -35

* A program directly alters the contents of BLK
  ---------------------------------------------
  Ignore and continue: the input source will be switched to the following
  block the next time that it becomes exhausted. This could cause confusion
  in the system if the current source is not a block (ie the unmodified 
  contents of BLK were zero). If a program alters BLK to zero from a non-zero
  value, confusion will definitely reign...

* No current block buffer for UPDATE
  ----------------------------------
  Ignore and continue: this may result in an error attempting to write block 0.


Other system documentation: BLOCK and BLOCK EXTENSION word sets
===============================================================

* Restrictions due to multiprogramming system on buffer addresses
  ---------------------------------------------------------------
  Buffers are shared between all tasks (as are all other data areas).
  Therefore, after a PAUSE, all buffer addresses previously obtained
  are invalid.

* Number of blocks available for source text and data
  ---------------------------------------------------
  Limited only by available operating system resources


Implementation-defined options: EXCEPTION and EXCEPTION EXTENSION word sets
===========================================================================

* Values used in the system by CATCH and THROW
  --------------------------------------------
   Standard exceptions:
	-1		ABORT
	-2		ABORT"
	-13		undefined word
	-35		invalid block number
	-49		search-order overflow
   System-specific exceptions:
	-4095		word cannot be "fast"-compiled
	-4094		input nesting exception
	-4093		non-contigious compilation spaces
	-4092		invalid block buffer allocation
        -(256+DSSerr)   DSS error code (see DSS documentation for details)


Implementation-defined options: FACILITY and FACILITY EXTENSION word sets
=========================================================================

* Encoding of keyboard events by EKEY
  -----------------------------------
  Key events are encoded as follows:
    0..255	standard ASCII codes and control characters, including ENTER
                BACKSPACE TAB and ESC
    256..511	subtracting 256 from this value leaves a special key code
                returned by the DSS call WAITKEY. Values for specific keys
                can be determined by experimentation.

* Duration of a system clock tick
  -------------------------------
  Undefined, but resolution of MS is 1/1000s (one millisecond)

* Repeatability to be expected from execution of MS
  -------------------------------------------------
  This word is implemented using an accurate machine-coded delay loop. However,
  a certain amount of variation can be expected, depending on the current load
  placed on the system interrupts. When in multi-programming mode, this word
  executes PAUSE every 10ms and therefore becomes wildly inaccurate if any
  other tasks are awake.
  If the Sprinter is not in Turbo mode, this word will execute approximately
  three times slower than expected.


Ambiguous conditions: FACILITY and FACILITY EXTENSION word sets
===============================================================

* AT-XY operation can't be performed on user output device
  --------------------------------------------------------
  The operation is simply ignored with no effect


Implementation-defined options: FILE and FILE EXTENSION word sets
=================================================================

* File access methods used
  ------------------------
  The R/O R/W amd W/O file access methods are implemented using the
  equivalent Sprinter methods, except that for CREATE-FILE the W/O
  method is always used.
  The BIN method is a no-operation, as binary and text files are
  indistinguishable on the Sprinter.

* File exceptions
  ---------------
  None

* File line terminator
  --------------------
  READ-LINE accepts either CR (ASCII 13) or LF (ASCII 10) as a terminator.
  For files with compound (CRLF or LFCR) line terminators, each line returned
  by READ-LINE will be followed by an additional line of zero length.

* File name format
  ----------------
  This follows the standard 8.3 Sprinter filename conventions detailed in
  the Sprinter documentation and may include drive letters and directory
  paths. File names are case-insensitive.

* Information returned by FILE-STATUS
  -----------------------------------
  The value returned is the attributes value for the file, as described
  in the Sprinter documentation. This can be used to determine if a "file"
  is a file or directory, for example.

* Input file state after an exception
  -----------------------------------
  When an exception is THROWn, any files currently being interpreted by
  INCLUDED or INCLUDE-FILE that are exited by the exception are automatically
  closed.

* IOR values and meaning
  ----------------------
  All ior values returned are the result of adding 256 to the DSS error
  received and then negating the result, ie -(256+DSSerr).

* Maximum depth of file input nesting
  -----------------------------------
  Up to 8 nested levels are permissible. However, this number of levels will
  overwrite PAD; for safety it is recommended that a maximum of 6 nested
  levels are used.

* Maximum size of input line
  --------------------------
  128 characters

* Methods for mapping block ranges to files
  -----------------------------------------
  No block buffers are allocated initially, so this must be done first using:
	BLKS ( n -- )
  This allocates n block buffers for use (n=1..4) with the system, and may
  only be performed once, or exception -4092 will be THROWn.

  Prior to using any words from the BLOCK and BLOCK EXTENSION word sets, an
  existing file must first be mapped to one of four available ranges using the
  word:		MAP ( caddr u n -- )
  Where caddr u is a filename, and n specifies a range of blocks to map the
  file to as follows:
	n=0	Blocks     1...4096
	n=1	Blocks  4097...8192
	n=2	Blocks  8193..12288
	n=3	Blocks 12289..16384

  Up to 4 files can be mapped at once, one to each of the available ranges.
  To unmap a file from a range of blocks, use the following word:
		UNMAP ( n -- )
  which unmaps the file just unmapped. This must be done before exiting
  CamelForth, to avoid leaving files open.

  Note that use of MAP and UNMAP always invalidate ALL the block buffers.
  One final point to make is that there is no correlation between the
  number of block buffers allocated and the number of files mapped; the
  system will work just as well with 4 buffers and 1 file as with 1 buffer
  and 3 files, or any other combination.

* Number of string buffers provided by S"
  ---------------------------------------
  1

* Size of string buffer used by S"
  --------------------------------
  128 characters


Ambiguous conditions: FILE and FILE EXTENSION word sets
=======================================================

* Attempting to position a file outside its boundaries
  ----------------------------------------------------
  Ignore and continue: an error may occur during read/write operations

* Attempting to read from file positions not yet written
  ------------------------------------------------------
  The system may crash

* Fileid is invalid
  -----------------
  An ior of -261 (corresponding to DSS error 5 - invalid handle) is
  usually returned

* Named file cannot be opened
  ---------------------------
  Exception -259 is THROWn (corresonding to DSS error 3 - file not found)

* Requesting an unmapped block number
  -----------------------------------
  If no file is mapped to blocks, exception -35 will be THROWn. Otherwise,
  the file will be extended as necessary to allow mapping to the requested
  block number (if this causes a file error, the ior will be THROWn as
  an exception).

* Using SOURCE-ID when BLK is not zero
  ------------------------------------
  Under these circumstances SOURCE-ID will return zero.


Implementation-defined options: PROGRAMMING TOOLS and EXTENSION word sets
=========================================================================

* Ending sequence for input following CODE
  ----------------------------------------
  The word NEXT will compile a jump to the inner interpreter, forming a
  suitable ending sequence.

* Manner of processing input following CODE
  -----------------------------------------
  No assembler is provided, so code must be hand-assembled and compiled into
  the dictionary with the C, and , words.

* Search-order capability for EDITOR
  ----------------------------------
  The ANS Forth Search-Order word set.


Implementation-defined options: SEARCH-ORDER and EXTENSION word sets
====================================================================

* Maximum number of word lists in the search order
  ------------------------------------------------
  8

* Minimum search order
  --------------------
  FORTH


Ambiguous conditions: SEARCH-ORDER and SEARCH-ORDER EXTENSION word sets
=======================================================================

* Changing the compilation word list
  ----------------------------------
  This has no adverse affects, since the definition is entered into the
  compilation word list before it can be changed, and any changes made by
  IMMEDIATE and the like apply to the latest definition, irrespective of
  the compilation word list.

* Search order empty
  ------------------
  Ignore and continue: this will cause a crash.

* Too many word lists in search order
  -----------------------------------
  Exception -49 (search order overflow) is THROWn.


